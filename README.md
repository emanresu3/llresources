# LibreLingo Resources

A set of templates and resources I use for developing LibreLingo courses

## Other resources

https://en.m.wiktionary.org/wiki/Wiktionary:Frequency_lists/

## Related

[llgenerate](https://codeberg.org/emanresu3/llgenerate)

## License

Any software in this repository will be under the AGPL-3.0 license unless explicitly noted otherwise.
Any other resource (like the YAML files) will be under the CC-BY-SA 4.0 license unless explicitly noted otherwise.
